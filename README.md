# Hardlink Maker


## What is Hardlink Maker? 
> Hardlink Maker allows user to create Hard links to external Libraries or Code out of Assets folder to share resources between multiple Unity projects.


Only works on Windows by default.

## Quickstart

Before you start, you may need to install a bash console if you are in windows (The [Git Bash Console](https://gitforwindows.org/) is enough)


Also, you must put the Bash location as environment variable as BASH_PATH.


1. Open ```Packages/manifest.json``` and add at the end of the file:
    ```
    "com.lurtis.hardlinkmaker":  "https://bitbucket.org/lurtisrules/com.lurtis.hardlinkmaker.git"
    ```
    
    Also, you cand add the project at Window > Package Manager > "+" and paste the URL: https://bitbucket.org/lurtisrules/com.lurtis.hardlinkmaker.git

2. Open the window at ```Lurtis4Unity / Hardlink Maker```


3. Select the folder you want to add to your project and click on ```Create Hardlinks``` button.


