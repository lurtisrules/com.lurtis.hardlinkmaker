﻿// From: https://answers.unity.com/questions/1376497/how-do-i-find-a-scripts-location-on-the-hard-drive.html
using System.IO;
using System.Linq;
using UnityEngine;

public static class ExternalFilesFinder
{
    public static string FindPathAndFileByClassName(string className)
    {
        return FindAFileInADirectoryRecursively($"{Application.dataPath}/../", className);
    }

    private static string FindAFileInADirectoryRecursively(string startDir, string findName)
    {
        foreach (var file in Directory.GetFiles(startDir))
        {
            var words = file.Split(new char[] { '/', '\\' });
            if (words.Any(t => t == findName))
                return file;
        }

        return Directory.GetDirectories(startDir)
            .Select(dir => FindAFileInADirectoryRecursively(dir, findName))
            .FirstOrDefault(strBuff => strBuff != null)?
            .Replace('\\', '/');
    }
}

