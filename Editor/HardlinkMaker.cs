﻿using System;
using System.Diagnostics;
using System.IO;
using UnityEditor;
using UnityEngine;
using Debug = UnityEngine.Debug;

namespace Hardlinkmaker.Scripts
{
    public class HardlinkMaker : EditorWindow
    {
        private const string ButtonLabel = "Create Hardlinks";
        private const string TitleLabel = "Hardlink Maker";
        private const string MenuItemName = "Lurtis4Unity/Hardlink Maker";
        private const string OriginPathLabel = "Origin: ";
        private const string DestinationPathLabel = "Destination: ";
        private const string SearchLabel = "...";
        private const string SelectFolderButtonLabel = "Select Folder";
        private const string BashFileName = "hardlinkmaker.sh";

        private static string BashPath => Environment.GetEnvironmentVariable("BASH_PATH");

        private string _originPath;
        private string _destinationPath;

        private bool _isSharedLinked;
        private bool _isKitchenLinked;

        [MenuItem(MenuItemName)]
        public static void ShowWindow()
        {
            GetWindow(typeof(HardlinkMaker));
        }

        private void Awake()
        {
            this._originPath = DefaultOriginDirectory();
            this._destinationPath = Application.dataPath;
            this._isSharedLinked = true;
        }

        private void OnGUI()
        {
            GUILayout.Label(TitleLabel, EditorStyles.boldLabel);
            EditorGUILayout.Space(10.0f);
            OriginSelector(OriginPathLabel, ref this._originPath);
            EditorGUILayout.Space(20.0f);
            OriginSelector(DestinationPathLabel, ref this._destinationPath);
            EditorGUILayout.Space(20.0f);
            CustomHardlinksSelector();
            EditorGUILayout.Space(20.0f);
            GenerateHardlinksButton();
        }

        private void CustomHardlinksSelector()
        {
            EditorGUILayout.BeginHorizontal();
            EditorGUI.BeginDisabledGroup(true);
            this._isSharedLinked = EditorGUILayout.ToggleLeft(" Shared", this._isSharedLinked);
            EditorGUI.EndDisabledGroup();
            this._isKitchenLinked = EditorGUILayout.ToggleLeft(" Kitchen", this._isKitchenLinked);
            EditorGUILayout.EndHorizontal();
        }

        private void OriginSelector(string pathLabel, ref string path)
        {
            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.LabelField(pathLabel, path);
            if (GUILayout.Button(SearchLabel, GUILayout.Width(50.0f)))
            {
                path = EditorUtility.OpenFolderPanel(SelectFolderButtonLabel, "", "");
                this.Repaint();
            }

            EditorGUILayout.EndHorizontal();
        }

        private void GenerateHardlinksButton()
        {
            if (GUILayout.Button(ButtonLabel))
            {
                if (!IsBashPathSet() || !IsPathValid()) return;
                var processStartInfo = ProcessStartInfo();
                var process = Process.Start(processStartInfo);
                process.OutputDataReceived += (sender, args) => UnityEngine.Debug.Log(args.Data); // do whatever processing you need to do in this handler
                process?.WaitForExit();
            }
        }

        private static bool IsBashPathSet()
        {
            if (string.IsNullOrEmpty(BashPath))
            {
                Debug.LogError("BASH_PATH has not been set as an environment variable.");
                return false;
            }

            return true;
        }

        private bool IsPathValid()
        {
            if (string.IsNullOrEmpty(this._originPath) || !Directory.Exists(this._originPath))
            {
                Debug.LogError("Path selected is not valid.");
                return false;
            }

            return true;
        }

        private ProcessStartInfo ProcessStartInfo()
        {
            var fullPath = ExternalFilesFinder.FindPathAndFileByClassName(BashFileName);
            var newHardlinkDirectory = $"{this._destinationPath}/{Path.GetFileName(this._originPath)}";
            var processStartInfo = new ProcessStartInfo
            {
                FileName = BashPath,
                UseShellExecute = false,
                RedirectStandardOutput = false,
                Arguments = $"\"{fullPath}\" \"{this._originPath}\" \"{newHardlinkDirectory}\" \"{Convert.ToByte(this._isKitchenLinked)}\""
            };

            return processStartInfo;
        }

        private static string DefaultOriginDirectory()
        {
            for (var i = 1; i <= 5; i++)
            {
                var backFolders = BackFoldersFrom(i);
                var path = Path.GetFullPath(Application.dataPath + backFolders + "/aecassets");
                if (Directory.Exists(path))
                    return path;
            }

            return "";
        }

        private static string BackFoldersFrom(int index)
        {
            var backFolders = "";
            for (var j = 1; j <= index; j++)
                backFolders += "/..";
            return backFolders;
        }
    }
}
