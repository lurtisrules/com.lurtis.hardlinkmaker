#FROM: https://gist.github.com/xombiemp/4748562
#!/bin/bash
oldifs=$IFS
IFS='
'
[ $# -ne 3 ] && { echo "Usage: $0 sourceDirectory destinationDirectory" ; exit 1; }
[ ! -d "$1" ] && { echo "$1 is not a valid directory"; exit 1; }
[ ! -d "$2" ] && { mkdir -p "$2"; }
src=$(cd "$1" ; pwd)
dst=$(cd "$2" ; pwd)

isKichenLinked=$3
if [ "$isKichenLinked" -eq 1 ]; then

	find "$src" -type d | grep -v "\.git" |
	while read dir; do
			mkdir -p "$dst${dir#$src}"
	done


	find "$src" -type f -o -type l | grep -v "\.git" |
	while read src_f; do
			dst_f="$dst${src_f#$src}"
			ln "$src_f" "$dst_f"
	done
	
else

	find "$src" -type d | grep -v "\.git" |  grep -v "\Kitchen" |
	while read dir; do
			mkdir -p "$dst${dir#$src}"
	done


	find "$src" -type f -o -type l | grep -v "\.git" |  grep -v "\Kitchen" |
	while read src_f; do
			dst_f="$dst${src_f#$src}"
			ln "$src_f" "$dst_f"
	done
	
fi

IFS=$oldifs


#$1 source
#$2 destiny
#$3 kitchen?

#bash hardlinkmaker.sh "C:\Users\ntapi\Documents\Unity Projects\AECAssets\Assets\aecassets\Resources" "C:\Users\ntapi\Documents\Unity Projects\HardlinkMaker\Assets\Resources" 1
#bash hardlinkmaker.sh "C:\Users\ntapi\Documents\Unity Projects\AECAssets\Assets\aecassets\Resources" "C:\Users\ntapi\Documents\Unity Projects\HardlinkMaker\Assets\Resources" 0


